<?php

namespace App\Filament\Resources;

use App\Filament\Resources\HorsesResource\Pages;
use App\Filament\Resources\HorsesResource\RelationManagers;
use App\Models\Horses;
use App\Models\Horsescoat;
use App\Models\Horsesrace;
use App\Models\Horsessex;
use App\Models\User;
use Filament\Tables\Actions\Action;
use Filament\Forms\Components\Actions;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Hidden;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Section;
use Filament\Notifications\Notification;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HorsesResource extends Resource
{
    protected static ?string $model = Horses::class;
    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';
    protected static ?string $navigationLabel = 'Chevaux';
    protected static ?string $navigationGroup = 'Mes chevaux & cavaliers';

    public static function form(Form $form): Form
    {
        $user = Auth::user(); // Get the authenticated user
        $userId = $user->id; // Assuming 'id' is the user ID field in your user mode

        return $form
            ->schema([
                Section::make("Actions")
                    ->columns([
                        'sm' => 3,
                        'xl' => 3,
                        '2xl' => 4,
                    ])
                    ->schema([
                        Forms\Components\Actions::make([
                            Forms\Components\Actions\Action::make('Ajout Observation')
                                ->form([
                                    Select::make('authorId')
                                        ->label('Author')
                                        ->options(User::query()->pluck('name', 'id'))
                                        ->required(),
                                ])
                                ->action(function (array $data, Post $record): void {
                                    $record->author()->associate($data['authorId']);
                                    $record->save();
                                })
                        ]),
                        Placeholder::make('Nom')
                            // ->content(function (Inscriptionsmodeconcours $record) {
                            //     $value = DB::table('cavaliers')
                            //         ->where('id', '=', $record->cavalierid)->get();

                            //     $v = $value[0]->prenom . " " . $value[0]->nom;
                            //     return ($v);
                            // }),
                            ->content('cc'),
                        Placeholder::make('Epreuve')
                            ->content('cc'),
                        Placeholder::make('Points')
                            ->content('cc'),

                    ]),
                Section::make("Fiche d'identité")
                    ->schema([
                        Hidden::make('ownerid')
                            ->default($userId),
                        FileUpload::make('imageprofile')
                            ->disk('s3')
                            ->image()
                            ->required()
                            ->imageEditor()
                            ->imageEditorEmptyFillColor('#000000'),
                        TextInput::make('name')
                            ->label('Nom du cheval')
                            ->required(),
                        Select::make('horsesex')
                            ->options(Horsessex::all()->pluck('sexname', 'id'))
                            ->required()
                            ->searchable()
                            ->label('Sexe du cheval'),
                        Select::make('horserace')
                            ->options(Horsesrace::all()->pluck('racename', 'id'))
                            ->required()
                            ->searchable()
                            ->label('Race du cheval'),
                        Select::make('horsecoat')
                            ->options(Horsescoat::all()->pluck('coatname', 'id'))
                            ->required()
                            ->searchable()
                            ->label('Robe du cheval'),
                        DatePicker::make('dob')
                            ->label('Date de naissance du cheval')
                            ->required(),
                        FileUpload::make('documents')
                            ->multiple()
                            ->label("Document d'identité")
                            ->disk('s3')
                            ->openable()
                            ->uploadingMessage('Uploading attachment...')
                            ->downloadable()
                            ->acceptedFileTypes(['application/pdf'])
                            ->required()
                            ->appendFiles()


                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {

        if (isset($_GET["status"])) {
            if ($_GET["status"] == "1") {
                Notification::make()
                    ->title("Il est obligatoire d'ajouter un cheval avant de s'inscrire")
                    ->danger()
                    ->send();
            }
        }
        $user = Auth::user(); // Get the authenticated user
        $userId = $user->id; // Assuming 'id' is the user ID field in your user mode

        return $table
            ->modifyQueryUsing(function (Builder $query) use ($userId) {
                $query->where('ownerid', $userId);
            })
            ->columns([
                ImageColumn::make('imageprofile')
                    ->disk('s3')
                    ->label('Photo'),
                TextColumn::make('name')
                    ->searchable()
                    ->sortable()
                    ->label('Nom du cheval')
                    ->weight('medium')
                    ->alignLeft(),
                // TextColumn::make('horsesex')
                //     ->formatStateUsing(function (Horses $record) {
                //         $value = DB::table('horsessexes')
                //             ->where('id', '=', $record->horsesex)->get();
                //         $v = $value[0]->sexname;
                //         return ($v);
                //     })
                //     ->searchable()
                //     ->sortable()
                //     ->label('Sexe du cheval')
                //     ->weight('medium')
                //     ->alignLeft(),
                // TextColumn::make('horserace')
                //     ->formatStateUsing(function (Horses $record) {
                //         $value = DB::table('horsesraces')
                //             ->where('id', '=', $record->horserace)->get();
                //         $v = $value[0]->racename;
                //         return ($v);
                //     })
                //     ->searchable()
                //     ->sortable()
                //     ->label('Race du cheval')
                //     ->weight('medium')
                //     ->alignLeft(),
                // TextColumn::make('horsecoat')
                //     ->formatStateUsing(function (Horses $record) {
                //         $value = DB::table('horsescoats')
                //             ->where('id', '=', $record->horsecoat)->get();
                //         $v = $value[0]->coatname;
                //         return ($v);
                //     })
                //     ->searchable()
                //     ->sortable()
                //     ->label('Robe du cheval')
                //     ->weight('medium')
                //     ->alignLeft(),
                // TextColumn::make('dob')
                //     ->searchable()
                //     ->sortable()
                //     ->label('Date de naissance du cheval')
                //     ->weight('medium')
                //     ->alignLeft(),

            ])
            ->filters([
                // SelectFilter::make('animal')
                //     ->multiple()
                //     ->options(
                //         Horses::select('animal')
                //             ->distinct()
                //             ->get()
                //             ->pluck('animal', 'animal')
                //     ),
            ])
            ->actions([
                // Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
                // Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                // Tables\Actions\BulkActionGroup::make([
                //     Tables\Actions\DeleteBulkAction::make(),
                // ]),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageHorsess::route('/'),
        ];
    }
}
